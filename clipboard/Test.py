import Clipboard
import Image


def run_clipboard_tests():

    def test_reset_clipboard():
        Clipboard.reset()
        assert(Clipboard.gettext() == None)
        assert(Clipboard.getblob() == None)


    def test_some_text():
        Clipboard.reset()
        Clipboard.puttext("Hello World")
        text = Clipboard.gettext()
        assert( text == "Hello World")

    
    def test_blob():
        Clipboard.reset()
        f = open("sandbucket.jpg")
        img = f.read()
        Clipboard.putblob(img)
        blob = Clipboard.getblob()
        assert(blob == img)



    test_reset_clipboard()
    test_some_text()
    test_blob()

run_clipboard_tests()
